const Projects = [{
	id: 0,
    title: "Tetris.js",
    description: "A Tetris clone soley built in React.js<br/><br/>The design and premise of the game is heavly influenced by <br/>the PS4 game: Tetris Effect.",
    filter: "Game",
    type: [{
        title: 'Game',
        tags: ['Design', 'Reactjs', 'Game']
    }],
    metadata:[{
        title: 'Category',
        text: 'Game [React.js]'
    },{
        title: 'Date',
        text: null
    },{
        title: 'Year',
        text: null
    },{
        title: 'Skills',
        text: 'Css, Javascript'
    },{
        title: 'Role',
        text: null
    },{
        title: 'Client',
        text: null
    },{
        title: 'Product',
        text: null
    },{
        title: 'Started',
        text: null
    },{
        title: 'Launch',
        text: null
    },{
        title: 'Completed',
        text: null
    },{
        title: 'Release',
        text: '22nd January 2020'
    },{
        title: 'Website',
        text: null
    }],
    url: '/portfolio/2/tetris',
    client: 'Personal project',
    role: 'Developer',
    mainImage: "/assets/images/tetris.jpg",
    gallery: [
        '/assets/images/infinity.jpg',
        '/assets/images/infinity1.jpg',
        '/assets/images/infinity2.jpg',
        '/assets/images/infinity3.jpg',
        '/assets/images/infinity4.jpg',
    ],
    projectInfo: [{
        title: 'Why Tetris?',
        text: 'After working primarily on web applications and WPA sites, my main inspiration was that I saw it as a challenege. A way that would allow me to enhance my javascript and css skills.',
        url: null
    },{
        title: 'Unleashing Creativity',
        text: 'With Reactjs and thirdparty libraries such as three.js or d3.js; it opens up the creative opportunity to attempt to replicate the PS4 Tetris effect game background scenes',
        url: null
    },{
        title: 'Shaking that grid',
        text: 'Utilising css properties, I added subtle movements to replicate the original game for pushing the Tetrinomino block to either side to shunt the level grid in the X & Y axis.',
        url: null
    }],
    info: {
        title: 'Main Features',
        list: [
            'Classic Tetris gameplay',
            'Title screen and score board',
            'Sound effects, music with level crossfade',
            'User interactable dialog boxes',
            "Power ups & score",
            'Connectivity to google firebase store',
            'Add your initals as a new high scorer',
            'Subtle gameplay fx'
        ]
    }
},{
	id: 1,
    filter: "App",
    title: 'Expensify',
    type: ['ReactJs', 'Web application'],
    banner: '/assets/images/expensify/expensify03.jpg',
    img: "/assets/images/expensify/expensify02.png",
    metadata:[{
        title: 'Category',
        text: 'Web app'
    },{
        title: 'Date',
        text: null
    },{
        title: 'Year',
        text: null
    },{
        title: 'Skills',
        text: 'Css / Javascript'
    },{
        title: 'Role',
        text: null
    },{
        title: 'Client',
        text: null
    },{
        title: 'Product',
        text: null
    },{
        title: 'Started',
        text: null
    },{
        title: 'Launch',
        text: null
    },{
        title: 'Completed',
        text: '26th March 2020'
    },{
        title: 'Release',
        text: null
    },{
        title: 'Website',
        text: null
    }],
    shortDesc: 'Running applicaton to track burn amount.',
    description: `An expense tracking application built with react allowing the user to add/remove income & expenditure; along with giving the ability to search for expenses by date or month. <br/><br/>The application utilises the google authentication signup and firebase integration allowing for mulitple users to use the application and store their information in a database.<br/><br/>
    <p class="project-desc-subtitle">Features</p>
        <ul class="project-desc-list">
            <li> <span>Registration and login forms</span> </li>
            <li> <span>Ability to add income</span></li>
            <li> <span>Ability to edit expenditure fields</span></li>
            <li> <span>Draggable history modal window</span></li>
            <li> <span>Filter bar: Sort by date or expenditure amount</span></li>
            <li> <span>Automated balance</span></li>
        </ul>`,
    navLink: 'expensify',
    year: '2020',
    skills: 'Photoshop',
    url: '/portfolio/1/expensify',
    gallery: [
        '/assets/images/expensify/expensify00.jpg',
        '/assets/images/expensify/expensify01.jpg',
        '/assets/images/expensify/expensify02.jpg',
        '/assets/images/expensify/expensify03.jpg'
    ]
},{
	id: 3,
    filter: "Design",
    title: 'Run Maximizer',
    type: ['App design'],
    banner: "/assets/images/runmaximizer/maximiser.jpg",
    metadata:[{
        title: 'Category',
        text: 'Design'
    },{
        title: 'Date',
        text: null
    },{
        title: 'Year',
        text: null
    },{
        title: 'Skills',
        text: 'Photoshop'
    },{
        title: 'Role',
        text: null
    },{
        title: 'Client',
        text: null
    },{
        title: 'Product',
        text: null
    },{
        title: 'Started',
        text: null
    },{
        title: 'Launch',
        text: null
    },{
        title: 'Completed',
        text: '19th March 2015'
    },{
        title: 'Release',
        text: null
    },{
        title: 'Website',
        text: null
    }],
    shortDesc: 'Running applicaton to track burn amount.',
    description: `
    <p>With an interest in Sport and with many personal health/running monitoring tracker applications available to download on iOS & Android platforms, I decided to create my own design for calculating the users current location to a way point/goal, with the addition of an area displaying calories burnt. </p>    <div className='feature-list'>
        <p class="project-desc-subtitle">Potential application features</p>
        <ul class="project-desc-list">
            <li> <span>Calorie burner, measured by GPS and speed</span> </li>
            <li> <span>Range to target/goal</span></li>
            <li> <span>Distance travelled</span></li>
            <li> <span>Progress bar to indicate completion of current goal</span></li>
            <li> <span>Runners current speed monitored by GPS</span></li>
            <li> <span>Elasped time since initated</span></li>
            <li> <span>Built in MP3 player</span></li>
        </ul>
    </div>`,
    img: "/assets/images/runmaximizer/work18.jpg",
    navLink: 'Run-maximiser',
    year: '2016',
    skills: 'Photoshop',
    url: '/portfolio/1/run-maximizer',
    client: 'E-HR',
    gallery: [
        '/assets/images/runmaximizer/maximiser00.jpg',
        '/assets/images/runmaximizer/maximiser01.jpg'
    ]
},{
	id: 4,
    filter: "Model",
    title: 'Pickle Rick',
    banner: "/assets/images/picklerick/rick-banner.jpg",
    type: ['Model'],
    metadata:[{
        title: 'Category',
        text: 'Model'
    },{
        title: 'Date',
        text: null
    },{
        title: 'Year',
        text: null
    },{
        title: 'Skills',
        text: '3D Modelling, Photoshop'
    },{
        title: 'Role',
        text: null
    },{
        title: 'Client',
        text: null
    },{
        title: 'Product',
        text: null
    },{
        title: 'Started',
        text: null
    },{
        title: 'Launch',
        text: null
    },{
        title: 'Completed',
        text: '19th December 2019'
    },{
        title: 'Release',
        text: null
    },{
        title: 'Website',
        text: null
    }],
    shortDesc: 'Pickle Rick!!!.',
    description: `Character from the hit animated show 'Rick & Morty, I created this model of Pickle Rick just for fun.`,
    mainImage: "/assets/images/picklerick/rick.png",
    url: '/portfolio/1/pickle-rick',
    year: '2019',
    skills: 'Photoshop',
    projectInfo:[],
    gallery: [
        '/assets/images/picklerick/rick0.jpg',
        '/assets/images/picklerick/rick1.jpg'
    ]
},{
	id: 4,
    filter: "Design",
    title: 'Xtreme Revolution - Studios',
    banner: "/assets/images/xrs/000.jpg",
    type: ['Model'],
    
    metadata:[{
        title: 'Category',
        text: 'Design'
    },{
        title: 'Date',
        text: null
    },{
        title: 'Year',
        text: null
    },{
        title: 'Skills',
        text: 'Photoshop'
    },{
        title: 'Role',
        text: null
    },{
        title: 'Client',
        text: 'Xtreme Revolution - Studios'
    },{
        title: 'Product',
        text: null
    },{
        title: 'Started',
        text: null
    },{
        title: 'Launch',
        text: null
    },{
        title: 'Completed',
        text: '3rd November 2016'
    },{
        title: 'Release',
        text: null
    },{
        title: 'Website',
        text: null
    }],
    shortDesc: '.',
    description: `Tasked with creating a new Hero slider design and branding for the company`,
    mainImage: "/assets/images/xrs/000.jpg",
    url: '/portfolio/1/xrs',
    year: '2016',
    skills: 'Photoshop',
    projectInfo:[],
    gallery: [
        '/assets/images/xrs/000.jpg',
        '/assets/images/xrs/xrs-mug.jpg',
        '/assets/images/xrs/xrs-pen.jpg'
    ]
},{
	id: 4,
    filter: "Game",
    title: 'Ashura: Dark reign',
    banner: "/assets/images/adr/adr01.jpg",
    type: ['Model'],
    metadata:[{
        title: 'Category',
        text: 'Game'
    },{
        title: 'Date',
        text: null
    },{
        title: 'Year',
        text: null
    },{
        title: 'Skills',
        text: '3D Modelling, Photoshop, Level design, animation'
    },{
        title: 'Role',
        text: null
    },{
        title: 'Client',
        text: null
    },{
        title: 'Product',
        text: null
    },{
        title: 'Started',
        text: null
    },{
        title: 'Launch',
        text: null
    },{
        title: 'Release',
        text: '1st January 2008'
    },{
        title: 'Completed',
        text: '28th November 2008'
    },{
        title: 'Website',
        text: null
    },{
        title: 'Engine',
        text: 'Unreal Engine 2.4'
    }],
    shortDesc: 'ADR',
    description: `Ashura: Dark reign is a Sonic the hedgehog fan game that I was project lead of a team of 8, and took on other various roles in modeling, texturing, level design and animation.<br/><br/>
    ADR was created as a total conversion from the game 'Unreal Tournament 2004' by altering the physics and stripping the original FPS gameplay and transforming it into a 3rd person adventure game.
    Gameplay followed the standard Sonic game formula. Collect rings, enter special stages, obtain Chaos emeralds, smash Badniks(Robots) across 1 level spanned across 3 acts with differing environments followed by a classic boss battle with Dr. Robotnik/Eggman. <br/><br/>The character of Ashura (seen above & below) was co created by myself and Ian 'PunkXBlaze' Linn back in 2005 and has since become a major fan favorite character design within the Sonic fan community.`,
    mainImage: "/assets/images/picklerick/rick.png",
    url: '/portfolio/1/adr',
    year: '2019',
    skills: 'Photoshop',
    projectInfo:[],
    gallery: [
        '/assets/images/adr/adr03.jpg',
        '/assets/images/adr/adr00.jpg',
        '/assets/images/adr/adr01.jpg',
        '/assets/images/adr/adr02.jpg',
        '/assets/images/adr/adr04.jpg',
        '/assets/images/adr/PCGamerUK_ADR.jpg',
        '/assets/images/adr/angelCoast.jpg',
        '/assets/images/adr/masterEmerald.jpg',
        '/assets/images/adr/skySanctuary.jpg'
    ]
}
// 	id: 9,
//     title: 'Greystone Digital FX',
//     type: 'Design, Development',
//     description: "Company website for visual, audio company 'Greystone Digital FX based in Zimbabwe.'",
//     img: "/assets/images/portfolio/work9.jpg",
//     navLink: 'greystone',
//     year: '2017',
//     date: '27th January 2018',
//     tags: ['Design', 'Development'],
//     skills: 'css, html, javascript',
//     skills: 'Photoshop, HTML, Javascript',
//     url: '/portfolio/greystone',
//     client: 'Greystone Digital FX',
// },{
// 	id: 10,
//     title: 'MonkeyFish Development',
//     type: 'Web design',
//     description: "Lorum ipsum consecturere eli iset elit durem.",
//     img: "/assets/images/portfolio/work11.jpg",
//     navLink: 'monkeyfish',
//     year: '2016',
//     skills: 'Photoshop',
//     url: '/portfolio/monkeyfish',
//     client: 'E-HR',
//     gallery: [
//         '/assets/images/portfolio/work3.jpg',
//         '/assets/images/portfolio/work1.jpg'
//     ]
// },{
// 	id: 11,
//     title: 'Unreal Championship 2004',
//     type: '3DS Max, Unreal engine 2',
//     description: "Modelled, rigged and animated the characters of Anubis and Necris Brock.",
//     img: "/assets/images/portfolio/work14.jpg",
//     navLink: 'uc2004',
//     year: '2005',
//     date: '25th January 2005',
//     skills: 'Photoshop',
//     url: '/portfolio/uc2004',
//     tags: ['3D Modelling', 'Animation'],
//     client: 'Total Adrenaline Productions',
//     gallery: [
//         '/assets/images/portfolio/work3.jpg',
//         '/assets/images/portfolio/work1.jpg'
//     ]
// },{
// 	id: 12,
//     title: 'Heaven of Relics',
//     type: '3DS Max, Unreal engine 2',
//     description: "Created reusable 3d model asset; textured and animated for Unreal Tournament 2007 videogame modification.",
//     img: "/assets/images/portfolio/work12.jpg",
//     navLink: 'heaven-of-relics',
//     year: '2009',
//     date: '13th March 2009',
//     skills: 'Photoshop, 3d modelling, animation',
//     url: '/portfolio/heaven-of-relics',
//     client: "Javier 'Xaklse' Osset",
//     tags: ['3D Modelling', 'Animation', 'Photoshop'],
//     gallery: [
//         '/assets/images/portfolio/work3.jpg',
//         '/assets/images/portfolio/work1.jpg'
//     ]
// },{
// 	id: 13,
//     title: 'ADR: Tech demo',
//     type: '3DS Max, Unreal engine 2',
//     description: "Technology demo show casing redefined physics of a Sonic the Hedgehog game within Unreal engine 2",
//     img: "/assets/images/portfolio/work13.jpg",
//     navLink: 'adr-techdemo',
//     year: '2008',
//     date: '28th November 2008',
//     skills: 'Photoshop, level',
//     url: '/portfolio/adr-techDemo',
//     client: 'Xtreme Revolution: Studios',
//     tags: ['Project lead', 'Level design', 'Various'],
//     gallery: [
//         '/assets/images/portfolio/work3.jpg',
//         '/assets/images/portfolio/work1.jpg'
//     ]
// },{
// 	id: 14,
//     title: 'Merlin Music',
//     type: 'Web design',
//     description: "Redesign proposal for the personal website of music instructor<br/>Peter Latham.",
//     img: "/assets/images/portfolio/work1.jpg",
//     navLink: 'merlin-music',
//     year: '2015',
//     date: '17th September 2015',
//     skills: 'Photoshop',
//     url: '/portfolio/merlin-music',
//     client: 'Peter Latham',
//     tags: ['Design'],
//     gallery: [
//         '/assets/images/portfolio/work3.jpg',
//         '/assets/images/portfolio/work1.jpg'
//     ]
// },{
// 	id: 15,
//     title: 'Thirst: Game',
//     type: 'UE4 Game',
//     description: "Lorum ipsum consecturere eli iset elit durem.",
//     img: "/assets/images/portfolio/work15.jpg",
//     navLink: 'thirst',
//     year: '2019',
//     skills: 'Photoshop',
//     url: '/portfolio/typography',
//     client: 'E-HR',
//     gallery: [
//         '/assets/images/portfolio/work3.jpg',
//         '/assets/images/portfolio/work1.jpg'
//     ]
// },{
// 	id: 16,
//     title: 'Slack Chat Application',
//     type: 'Design',
//     img: "/assets/images/portfolio/slack-app2.jpg",
//     description: "Dark theme Slack application redesign<br/><br/>Slack is a company based email replacement by utlising the old IRC <br/>(internet relay chat) system as a way to keep conversations organised.",
//     type: "Design, Development",
//     navLink: "slack",
//     year: '2020',
//     date: '23nd February 2020',
//     tags: ['Design', 'React.js', 'Firebase'],
//     skills: 'Photoshop, Javascript, Sass',
//     url: '/portfolio/slack',
//     client: 'Personal project',
//     gallery: [
//         '/assets/images/portfolio/work3.jpg',
//         '/assets/images/portfolio/work1.jpg'
//     ]
// },{
// 	id: 17,
//     title: 'Jaiss flyer',
//     type: 'Branding',
//     description: "Reopening of Club Jaiss in Florence, Italy",
//     img: "/assets/images/portfolio/work19.jpg",
//     navLink: 'club-jaiss',
//     year: '2019',
//     skills: 'Photoshop',
//     url: '/portfolio/typography',
//     client: 'E-HR',
//     gallery: [
//         '/assets/images/portfolio/work3.jpg',
//         '/assets/images/portfolio/work1.jpg'
//     ]
//}
]

export default Projects;