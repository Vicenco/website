import React from 'react';
import './ProjectPagination.scss';

const ProjectPagination = ({prevProject, nextProject}) => {

    const switchBackground = (image) => {

        if (image){
            document.getElementById("paginationContainer").style.backgroundImage = `url(${image})`;
        } else {
            document.getElementById("paginationContainer").style.backgroundImage = "none";
        }
        
    }


    return (
        <div id="paginationContainer" className="project-pagination-container">
        {
            prevProject ? (
                <div className="project-container" onMouseEnter={() => switchBackground(prevProject.mainImage)} onMouseLeave={() => switchBackground()}>
                    <a className="project-container-inner" href={prevProject.url}>
                        <div className="project-pagination-label-empty"></div>
                        <div className="project-pagination-info">
                            <div className="project-pagination-arrow">
                                <i className="demo-icon icon-cancel"/>
                            </div>
                            <h3 className="project-name">{prevProject.title}</h3>
                        </div>
                        <div className="project-pagination-label-prev">Prev</div>
                    </a>
                </div>
            ) : null
        }
        {
            nextProject ? (
                <div className="project-container" onMouseEnter={() => switchBackground(nextProject.mainImage)} onMouseLeave={() => switchBackground()}>
                    <a className="project-container-inner" href={nextProject.url}>
                        <div className="project-pagination-label-next">Next</div>
                        <div className="project-pagination-info">
                            <h3 className="project-name">{nextProject.title}</h3>
                            <div className="project-pagination-arrow">
                                <i className="demo-icon icon-cancel"/>
                            </div>
                        </div>                        
                    </a>
                </div>         
            ) : null
        }
        </div>
    );

};

export default ProjectPagination;