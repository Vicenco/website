import React from 'react';
import SecondMenu from './SecondMenu';
import './menupopover.css';

const MenuPopover = ({hideCanvas}) => {
 

    return (
        <div className="clb-popup" onClick={hideCanvas}>
            <div className="menu-btn" onClick={hideCanvas} >
                <div className="btn-round clb-close">
                    <i className="icon iconFont icon-cancel" />
                </div>
            </div>
            <div className="twin-container">
                <SecondMenu/>
            </div>
            
            
            <div className="clb-hamburger-nav-details">
                <div className="hamburger-nav-info">
                    <div className="hamburger-nav-info-item">
                        <span>Work Inquiries</span><br /> vinni@vicenco.co.uk<br /> Tel. +44.741.230.1354
                    </div>
                </div>
            </div>
        </div>
    );
};

export default MenuPopover;