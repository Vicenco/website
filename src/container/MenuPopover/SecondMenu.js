import React, { useState } from 'react';
import './secondmenu.css';

const MyMenu = [{
    label: "Home",
    url: "/",
    childNodes: null,
    hasChildren: false,
    visible: true
}, {
    label: "Portfolio",
    url: "/portfolio",
    childNodes: null,
    hasChildren: false,
    visible: true
}, {
    label: "About me",
    url: "/about-me",
    childNodes: null,
    hasChildren: false,
    visible: true
}, {
    label: "Blog",
    url: '/blog',
    childNodes: null,
    hasChildren: false,
    visible: false
}, {
    label: "Tutorials",
    url: '#',
    childNodes: [{
        label: "Reactjs",
        url: "/react",
        hasChildren: false
    }],
    hasChildren: true,
    visible: false
}, {
    label: "Store",
    url: '/store',
    childNodes: null,
    hasChildren: false,
    visible: false
}, {
    label: "Contact",
    url: "/contact",
    childNodes: null,
    hasChildren: false,
    visible: true
}]

const SecondMenu = () => {

    const [hovered, setHovered] = useState(false);
    const toggleHover = () => setHovered(!hovered);

    const createMenu = () => {

        let menuArray = []
        MyMenu.map((item, idx) => {

            if (item.visible && item.url != null) {

                if (!item.hasChildren) {

                    // Single menu item
                    menuArray.push(
                        <li key={idx} className="nav-item menu-item-depth-0">
                            <a href={item.url} className="menu-link main-menu-link item-title sub-nav" role="group" aria-expanded="false" aria-hidden="true">
                                <span className="">{item.label}</span>
                            </a>
                        </li>
                    )

                } else {

                    // Menu item with children
                    menuArray.push(

                        <li key={idx} className="nav-item menu-item-depth-0 has-submenu" onMouseEnter={toggleHover} onMouseLeave={toggleHover}>
                            <a href="#" className="menu-link main-menu-link item-title">
                                <span>{item.label}</span>
                                <div className="has-submenu-icon">
                                    <i className="demo-icon icon-cancel" />
                                </div>
                            </a>
                            <div className={hovered ? 'sub-nav visible' : 'sub-nav'} role="group" aria-expanded="true" aria-hidden="false">
                                <ul className="menu-depth-1 sub-menu sub-nav-group">
                                    { createSubMenu(item.childNodes) }
                                </ul>
                            </div>
                        </li>

                    )

                }

            }

        });

        return menuArray;

    };


    const createSubMenu = (subMenuArray) => {

        let subMenuItem = []
        subMenuArray.map((item, idx) => {

            if (item.url != null) {

                // Menu item with children
                subMenuItem.push(
                    <li key={idx} className="sub-nav-item menu-item-depth-1">
                        <a href={item.url} className="menu-link sub-menu-link">
                            <span className="subFont">Reactjs</span>
                        </a>
                    </li>
                )
            }

        });

        return subMenuItem;

    };


    return (

        <div className="clb-hamburger-nav">
            <div className="clb-hamburger-nav-holder" role="navigation">
                <ul id="secondary-menu" className="menu">
                    {createMenu()}
                </ul>
            </div>
        </div>
        
    );

};

export default SecondMenu;