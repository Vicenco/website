import React from 'react';
import './footer.scss';

function Footer() {
    return (
        <footer className="no-select">
            <span className="availablilty-text">Availability</span>
            <div className="copyright">
                © <a href="http://vicenco.co.uk" target="_blank" rel="noopener noreferrer">Vicenco</a>, { new Date().getFullYear() }
            </div>
        </footer>
    );
}

export default Footer;