import React from 'react';
import Logo from '../../components/Logo/Logo.component';
import Navigation from '../../components/Navigation/Navigation';
import './header.scss';


function Header() {
    return (
        <header id="masthead" className="site-header header-1 no-select" data-header-fixed="true" data-fixed-initial-offset="150">
            <Logo />
            <div className="right-part">
                <nav id="site-navigation" className="main-nav">
                    <Navigation/>
                </nav>                            
            </div>
        </header>
    );
}

export default Header;