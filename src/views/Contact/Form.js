import React from "react";
import Recaptcha from 'react-google-invisible-recaptcha';
import validateForm from './ValidateForm';
import useForm from './UseForm';
import './form.scss';

const Form = () => {

    const { handleChange, handleSubmit, values, errors } = useForm(submit, validateForm);
    let recaptcha;

    const options = [
        { value: 'less than 500', label: 'Less than 500' },
        { value: 'less than 1500', label: 'Less than 1500' },
        { value: 'less than 2500', label: 'Less than 2500' },
        { value: 'less than 3500', label: 'Less than 3500' },
        { value: 'more than 3500', label: 'More than 3500' },
    ];

    async function submit(){

        try {

            const token = await recaptcha.execute();
           
            // Perform check on recaptcha before submitting data.
            if (typeof token === 'string') {
                sendFeedback('template_z8tN5GP0',{
                    message_html: values.message,
                    from_name: values.name,
                    reply_to: values.email
                });
            }

        } catch (error) {
            console.log("Meh! Looks like a recaptch issue");
        }
        
    }

    const sendFeedback = (templateId, variables) => {

        window.emailjs.send('gmail', templateId, variables )
        .then(res => {
            console.log('Email successfully sent!')
        })
        .catch(err => console.error('Oh well, looks like theres an issue. Contact the site owner at "vincent.corleone@live.com" and report the following issue:', err))
    
    }


    return (

        <div>
            <form onSubmit={handleSubmit} noValidate>
                <div className="form-row">
                    <div className="field-wrapper">
                        <label htmlFor="name">Name</label>
                        <div>
                            <input 
                                className={`form-control ${ errors.name && "inputError" }`} 
                                type="text" 
                                name="name" 
                                value={values.name} 
                                onChange={handleChange} />
                            { errors.name && <span className="error-msg">{errors.name}</span> }
                        </div>
                    </div>
                    <div className="field-wrapper">
                        <label htmlFor="email">Email</label>
                        <div>
                            <input 
                                className={`form-control ${ errors.email && "inputError" }`} 
                                type="email" 
                                name="email" 
                                value={values.email} 
                                onChange={handleChange} />
                            { errors.email && <span className="error-msg">{errors.email}</span> }
                        </div>
                    </div>
                </div>
                <div className="form-row">
                    <div className="field-wrapper">
                        <label htmlFor="phone">Phone (optional)</label>
                        <div>
                            <input 
                                className={`form-control ${ errors.phone && "inputError" }`} 
                                type="text" 
                                name="phone" 
                                value={values.phone} 
                                onChange={handleChange} />
                            { errors.phone && <span className="error-msg">{errors.phone}</span> }
                        </div>
                    </div>
                    <div className="field-wrapper">
                        <label htmlFor="company">Company (optional)</label>
                        <div>
                            <input
                                className={`form-control ${ errors.company && "inputError" }`} 
                                type="text" 
                                name="company" 
                                value={values.company} 
                                onChange={handleChange} />
                            { errors.company && <span className="error-msg">{errors.company}</span> }
                        </div>
                    </div>
                    <div className="field-wrapper">
                        <label htmlFor="budget">Your budget (optional)</label>
                        <div>
                            <input 
                                className={`form-control ${ errors.budget && "inputError" }`} 
                                type="text" 
                                name="budget" 
                                value={values.budget} 
                                onChange={handleChange} />
                            { errors.budget && <span className="error-msg">{errors.budget}</span> }
                        </div>
                    </div>
                </div>
                <div className="form-row">
                    <div className="field-wrapper">
                        <label htmlFor="message">Your message</label>
                        <div>
                            <textarea 
                                className={`form-control ${ errors.message && "inputError" }`} 
                                type="text" 
                                name="message" 
                                value={values.message} 
                                onChange={handleChange} />
                            { errors.message && <span className="error-msg">{errors.message}</span> }
                        </div>
                    </div>
                </div>
                <div className="form-row">
                    <div className="row align-blockLeft">
                        <button type="submit" className="btn-square button ml-1">Submit</button>
                    </div>
                    <Recaptcha ref={ ref => recaptcha = ref } sitekey="6LcJH6kUAAAAAJoq4pOSNC7otJNfMPEaKdg_mSni" />
                </div>
            </form>
        </div>

    );

};

export default Form;
