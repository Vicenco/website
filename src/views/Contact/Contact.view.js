import React, { useState } from 'react';
import { useSpring, animated } from 'react-spring';
import SquareBtn from '../../components/Buttons/SquareBtn';
import Select from '../../components/Form/select/Dropdown';
import moment from 'moment';

import './contact.scss';
import Form from './Form';


const ContactPage = () => {

    const fade = useSpring({ from: { opacity: 0 }, opacity: 1, config: { duration: 900 } });

    const [focused, setfocused] = useState(false);

    return (
        <animated.div style={fade} className="container no-select">
            <div className="contact-section">
                <div className="contact-wrapper">
                    <div className="contact-header">
                        <h1 className="contact-header-title">Contact</h1>
                        <p className="contact-header-desc">I'm always looking to work alongside collaborative and ambitious partners.                            
                        I believe transparancy and communication help build the best relationships and ultimately; the best work. <br/><br/>
                        If you have a project you'd like to discuss, contact me at either of the options presented on this page.</p>
                        <div className="contact-block-item pt-3">
                            <h3>Get In Touch</h3>
                            <br/><a href="mailto:vinni@vicenco.co.uk?subject=Mail from Vicenco site">vinni@vicenco.co.uk</a>
                            <br/>Tel. +44.741.230.1354
                        </div>
                    </div>
                    <Form />
                </div>
            </div>
        </animated.div>
    );
};

export default ContactPage;