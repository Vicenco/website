export default function validateForm(values){

    let errors = {};

    // Name field validation
    if (!values.name){
        errors.name = "Your name is required"
    } else if (values.name.length < 3){
        errors.name = "Needs to be more than 3 characters";
    }

    // Email field validation
    if (!values.email){
        errors.email = "Email address is required"
    } else if (!/\S+@\S+\.\S+/.test(values.email)){
        errors.email = "Email address is invalid";
    }

    // Message field validation
    if (!values.message){
        errors.message = "Your message is required"
    } else if (values.message.length < 10){
        errors.message = "Needs to be more than 10 characters";
    }

    return errors;

}