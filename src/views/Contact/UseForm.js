import { useState, useEffect } from 'react';

const useForm = (callBack, validateForm) => {
    
    const [values, setValues] =  useState({ 
        name: "",
        email: "",
        phone: "",
        company: "",
        budget: "",
        message: ""
    });

    const [errors, setErrors] =  useState({});
    const [isSubmitting, setIsSubmitting] = useState(false);

    const handleChange = (oEvent) => {

        const {name, value} = oEvent.target;
        setValues({ ...values, [name]: value });
    }


    const handleSubmit = (oEvent) => {
        
        oEvent.preventDefault();

        // Handle errors
        setErrors(validateForm(values));
        setIsSubmitting(true);

    }


    useEffect(() => {

        // Check if there are no errors.
        if (Object.keys(errors).length === 0 && isSubmitting){
            callBack();
        }

    }, [errors]);

    return {
        handleChange,
        handleSubmit,
        values,
        errors
    };

}

export default useForm;