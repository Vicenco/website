import React, { useState } from 'react';
import { useSpring, animated } from 'react-spring';
import SquareBtn from '../../components/Buttons/SquareBtn';
import Select from '../../components/Form/select/Dropdown';
import moment from 'moment';

import './contact1.scss';


const ContactPage = () => {

    const fade = useSpring({ from: { opacity: 0 }, opacity: 1, config: { duration: 900 } });
    const [focused, setfocused] = useState(false);
    const [error, setError] = useState(null);
    const [formData, setFormData] = useState({

        createdAt: moment(),
        description: '',
        name: '',
        emailAddress: '',
        company: '',
        fone: '',
        budget: 'chocolate'

    });
    const options = [
        { value: 'chocolate', label: 'Chocolate' },
        { value: 'strawberry', label: 'Strawberry' },
        { value: 'vanilla', label: 'Vanilla' },
    ];

    /**
     * Handles change event for input, select and textarea fields.
     * 
     * @param {event} oEvent - Field parameters triggered by change event.
     * @private
     */
    const onValueChange = (oEvent) => {

        const sValue = oEvent.target.value;

        setFormData({ ...formData, [oEvent.target.name]: sValue });
        setError(null);

    };


    /**
     * Form submission.
     * 
     * @param {event} e - Triggered by user submit press.
     * @private
     */
    const onSubmit = (e) => {

        e.preventDefault();

        if (!formData.description || !formData.amount) {

            // Set error states.
            setError('Please provide a description & amount');

        } else {

            // Clear error states.
            setError(null);
            console.log("form submitted");

            // Submit form data.
            // onFormSubmit({
            //     description: formData.description,
            //     amount: parseFloat(formData.amount, 10) * 100,
            //     createdAt: formData.createdAt.valueOf(),
            //     note: formData.note
            // });

        }

    };

    return (
        <animated.div style={fade} className="container no-select">
            <div className="contact-section">
                <div className="contact-wrapper">
                    <div className="contact-header">
                        <h1 className="contact-header-title">Contact</h1>
                        <span className="contact-header-desc">I'm always looking to work alongside collaborative and ambitious partners. I believe transparancy and communication help build the best relationships and ultimately; the best work. If you have a project you'd like to discuss, let's start the great job.</span>
                        
                            <div className="contact-block-item pt-3">
                                <h3>Get In Touch</h3>
                                <br/><a mailto="vinni@vicenco.co.uk">vinni@vicenco.co.uk</a>
                                <br/>Tel. +44.741.230.1354
                            </div>
                    </div>
                    <form className="contactForm" onSubmit={(e) => onSubmit(e)}>

                        <div className="row">
                            <div className="fieldWrapper">
                                <label>Name (required)<br />
                                    <input
                                        type="text"
                                        name="name"
                                        className="form-control"
                                        placeholder="Your name"
                                        value={formData.name}
                                        onChange={(e) => onValueChange(e)}
                                    />
                                </label>
                            </div>
                            <div className="fieldWrapper">
                                <label>E-mail address (required)<br />
                                    <input
                                        type="email"
                                        name="emailAddress"
                                        className="form-control"
                                        placeholder="Your email address"
                                        value={formData.emailAddress}
                                        onChange={(e) => onValueChange(e)}
                                    />
                                </label>
                            </div>
                        </div>

                        <div className="row">
                            <div className="fieldWrapper">
                                <label>Contact number (Optional)<br />
                                    <input
                                        type="text"
                                        name="number"
                                        className="form-control"
                                        placeholder="Your contact number"
                                        autoFocus
                                        value={formData.fone}
                                        onChange={(e) => onValueChange(e)}
                                    />
                                </label>
                            </div>
                            <div className="fieldWrapper">
                                <label>Company (Optional)<br />
                                    <input
                                        type="text"
                                        name="company"
                                        className="form-control"
                                        placeholder="Your company name"
                                        value={formData.company}
                                        onChange={(e) => onValueChange(e)}
                                    />
                                </label>
                            </div>
                            <div className="fieldWrapper no-mr ">
                                <label>Your budget (required)<br />
                                    <select
                                        value={formData.budget}
                                        className="form-control"
                                        onChange={(e) => onValueChange(e)}>
                                        {
                                            options ? options.map((opt, idx) => (
                                                <option key={idx} value={formData.budget ? formData.budget : opt.value}>{opt.label}</option>
                                            )) : null
                                        }
                                    </select>
                                </label>
                            </div>
                        </div>

                        <div className="row">
                            <div className="fieldWrapper">
                                <label>Your message / Project details (required)<br />
                                    <textarea
                                        rows="4" cols="50"
                                        name="description"
                                        className="form-control"
                                        onChange={(e) => onValueChange(e)}
                                        value={formData.description}
                                        placeholder="Your message"
                                    />
                                </label>
                            </div>
                        </div>
                        <div className="row align-blockRight">
                            <SquareBtn className="button mr-1" label="Send" url="" />
                        </div>

                    </form>
                    {
                        error ? (
                            <div className="errorMessageBox">
                                <p>{error}</p>
                            </div>
                        ) : null
                    }
                </div>
            </div>
        </animated.div>
    );
};

export default ContactPage;