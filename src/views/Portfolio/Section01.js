import React from 'react';
import './section01.scss';

const Section01 = ({project}) => {
	return (
		<div className="section01-container">
			<div>
				<img src={project.mainImage} alt="image" className="section-image01" />
			</div>
			<div className="section-text">				
				<p className="context" dangerouslySetInnerHTML={{ __html: project.description }} />
			</div>
		</div>
	);
};

export default Section01;