import React from 'react';
import './headerA.scss';

const Header1 = ({ metadata }) => {
    return (
        <div className="section-meta-header section_pt">
            <div className="project-metadata">
                {
                    metadata ? (
                        metadata.map((item, idx) => {

                            if (item.text != null){

                                return (
                                    <div key={idx} className="meta-block">
                                        <div className="meta-heading">
                                            <h6 className="meta-title">{item.title}</h6>
                                        </div>
                                        <div className="meta-content">
                                            <ul className="meta-list">
                                                <li className="meta-item">
                                                    <span className="meta-description">{item.text}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                )
                            }
                        })
                    ) : null 
                }
            </div>
        </div>
    );
};

export default Header1;