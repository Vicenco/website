import React from 'react';
import './header2.scss';

const Header2 = ({ project }) => {
    return (
        <>
            <div className="triangle" />
            <div className="triangle" />
            <section className="header2-container">
                <div className="header-block">
                    <span className="meta">Project Phase</span>
                    <h2 className="title">{project.title}</h2>
                    <p className="context" dangerouslySetInnerHTML={{ __html: project.description }} />
                </div>
            </section>
        </>
    );
};

export default Header2;