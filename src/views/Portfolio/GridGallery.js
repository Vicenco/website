import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import styled from 'styled-components';
import { TimelineLite, Power3 } from 'gsap'; 
import PortfolioData from '../../portfolioData';
import LightBox from '../../components/Lightbox/Lightbox';

import './imageGallery.scss';

const GridGallery = ({project}) => {

    const [showLightBox, setLightBoxVisiblity] = useState(false);
    const [currentImage, setCurrentImage] = useState(false);
    const [loaded, hasLoaded] = useState(false);

    let tl = new TimelineLite({delay: .8});

    useEffect(()=> {
        
        if(!loaded){
           
            hasLoaded(true);
        }

    }, [tl]);

    const toggleLightBox = (oEvent) => {

        if(oEvent){
            setCurrentImage(oEvent.currentTarget.children[0].src);
        }        
        setLightBoxVisiblity(!showLightBox);

    }

    const closeLightBox = (oEvent) => {
        
        if(oEvent.target.id === "lightBox"){
            toggleLightBox();
        }

    }

    return (

        <div className="grid-gallery-portfolio">
            {
                project ? project.gallery.map((item, idx) => (
                    <GridItem key={idx} image={item} loaded={loaded} toggle={(e) => toggleLightBox(e)} />
                )) : null
            }            
            { showLightBox && <LightBox image={currentImage} closeBox={closeLightBox} /> }
        </div>

    )

};


const GridItem = ({image, toggle}) => {

    var classArray = [
        'big-box',
    ];
    const randomClass = Math.floor(Math.random() * classArray.length);
    
    return (
        <div className={`image-block ${classArray[randomClass]}`} onClick={toggle}>
            <img src={image} alt="image" onClick={toggle} />
            <div className="image-block__overlay">
                <span className="image-block__desc">View project details</span>
            </div>
        </div>
    );

};

export default GridGallery;