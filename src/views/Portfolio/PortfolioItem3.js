import React, { useState, useEffect } from 'react';
import './portfolio3.scss';

import portfolioData from '../../portfolioData';
import ProjectPagination  from '../../container/ProjectPagination/ProjectPagination';
import Header03 from './Header03';

const PortfolioItem3 = (props) => {

    const [project, setProject] = useState(null);
    const [prevProject, setPrevProject] = useState(null);
    const [nextProject, setNextProject] = useState(null);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        
        setLoading(true);
        const findProject = (idxValue) => {

            for (let idx = 0; idx < portfolioData.length; idx++) {
    
                const project = portfolioData[idx];
                if (project.id === idxValue){                
                    return(project);
                }
    
            }
    
        }
        for (let idx = 0; idx < portfolioData.length; idx++) {

            const project = portfolioData[idx];
            if (project.url === props.location.pathname){
                
                setProject(project);

                setNextProject(findProject(project.id +1));
                setPrevProject(findProject(project.id -1));

                setLoading(false);
                break;

            }

        }

        
    }, [props]);


    if (!project)
    {
        return null;
    }

    return (
        <div className="project" data="cocktail">
        <Header03 project={project} />
        {/* <div className="body" data-ibg-bg={project.mainImage}>
            <div className="bg project un-fixed cocktail scene-zoom scene"></div>
            <div className="gradient"></div>
        </div>
        <div className="container top-space">
            <div className="project-arrow scene">
                <div className="shaft"></div>
            </div>
        </div>
        <div className="container scene top-space" style={{zIndex:30}}>
            <div className="main-cont first">
                <div className="row">
                    <div className="project-title">
                        <h1>{project.title}</h1>
                    </div>
                </div>
                <div className="row project-details">                    
                    <div className="project-context pull-right">
                        <p className="skills-used">Print &nbsp;&nbsp;|&nbsp;&nbsp; Packaging &nbsp;&nbsp;|&nbsp;&nbsp; Photography</p>
                        <div className="short-hr"></div>
                        <p className="project-copy" dangerouslySetInnerHTML={{ __html: project.description }} />
                    </div>
                    <div className="col-right pull-right">
                        <h3>CREDITS / COOL PEOPLE:</h3>
                        <h3><span>Justin Durling<br/>Brent Schoepf<br/>Kelley Mann<br/>Colin Schye</span></h3>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="">
                    <img src="/assets/images/tetris.jpg" className="img-responsive"/>
                </div>
            </div>
            <div className="main-cont">
                <div className="row relative-row">
                    <div className="vertical-col">
                        <div className="short-vr"></div>
                        <h2>New Age, Old Fashioned.</h2>
                        <p className="project-copy">Each kit is inspired by a classic cocktail that evokes nostalgia, such as the Old Fashioned, the Dirty Martini, the Absinthe Louche, and so on. So what's included in the kit? Well, each cocktail kit supplies you with all the essential / natural ingredients to craft classic cocktails, just add your favorite alcoholic spirt and enjoy!</p>
                    </div>
                    <div className="vertical-col">
                        <img src="images/cocktail/cocktail-cup.svg" width="170" className="img-responsive"/>
                    </div>
                </div>
            </div>
            <div className="row image-block">
                <div>
                    <img src="images/cocktail/cocktail-5.jpg" className="img-responsive"/>
                </div>
                <div>
                    <img src="images/cocktail/cocktail-6.jpg" className="img-responsive"/>
                </div>
            </div>
            
            <div className="main-cont">
                <div className="row relative-row">
                    <div className="vertical-col">
                        <div className="short-vr"></div>
                        <h2>Cheers Friends.</h2>
                        <p className="project-copy">No happy hour is complete without good friends. I'd like to thank a few of my talented friends that were a part of Cocktail kits; Colin Schye for his extensive web skills, Kelley Mann and Brent Schoepf for their photography prowess, the team at Studio on Fire for their printing expertise, and Justin Durling for his dedication to great work, and for this awesome opportunity.</p>
                    </div>
                    <div className="vertical-col">
                        <img src="images/cocktail/cocktail-glass.svg" width="135" className="img-responsive"/>
                    </div>
                </div>
            </div>
            <div className="row image-block">
                <div className="">
                    <img src="images/cocktail/cocktail-14.jpg" className="img-responsive"/>
                </div>
            </div>
            
        </div> */}
        
    </div>
    );
};

export default PortfolioItem3;