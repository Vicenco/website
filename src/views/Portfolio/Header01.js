import React from 'react';
import './header01.scss';

import Metadata from '../../components/Metadata/MetaData';

const Header01 = ({project}) => {
    return (
        <div className="header-container">
            <img src="/assets/images/tetris/main.png" />
            <div className="project-header">
                <div className="project-metaTitle">
                    <h2 className="project-title">{project.title}</h2>
                    <span className="project-number">{`0${project.id +1}`}</span>
                    <span className="project-type">{project.filter}</span>
                </div>
                <Metadata tags={project.skills} date={null} />
                <p>
                Although many physical events around the world are on hold, there are plenty of places to connect with the Unreal Engine community online. From forums, to webinars, livestreams and full-on virtual events, our community of creators is continually staying active.
                </p>
            </div>
        </div>
    );
};

export default Header01;