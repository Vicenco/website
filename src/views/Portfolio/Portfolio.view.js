import React, { useState } from 'react';
import { useSpring, animated } from 'react-spring';

import DotGrid from '../../components/DotGrid/DotGrid';
import { TabButtonContainer, TabContent } from '../../components/VerticalScroller/verticalScroller.component';
import {TabGalleryButtonContainer} from '../../components/TabGallery/TabGallery.component';
import './portfolio.scss';

const Portfolio = () => {

    const fade = useSpring({ from: { opacity: 0 }, opacity: 1, config: { duration: 900 } });

    return (

        <animated.div style={fade} className="portfolio-container no-select">
            <DotGrid />
            <h2 className="page-label">Portfolio</h2>
            <TabGalleryButtonContainer title="My work" />
        </animated.div>
    );

}

export default Portfolio;