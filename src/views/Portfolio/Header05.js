import React from "react";
import "./header05.scss";
import GridGallery from "./GridGallery";
import Header01 from './Header1';

const Header05 = ({project}) => {

    if (!project)
    {
        return null;
    }
    
    return (
        <div className="header-container">
            <section className="section-wrapper section-wrapper-vertical">
                <div className="section-wrapper-content">
                    <img
                        src={project.banner}
                        alt="banner"
                        className="banner-image"
                    />
                </div>
            </section>
            <section className="section-wrapper">
                <div className="section-wrapper-content section-content-padding">
                    <div className="left-col">
                        <span className="subtitle">Project
                            <span>{`0${project.id +1}`}</span>
                        </span>
                        <h1 className="project-title">{project.title}</h1>
                    </div>
                    <div className="right-col group-content">
                        <div className="project-info">
                            <button className="btn-primary"></button>
                        </div>
                        <div className="markdown-wrapper">
                            <Header01 metadata={project.metadata} />
                        </div>
                    </div>
                </div>
                <div className="section-wrapper-content section-content-padding">
                    <div className="left-col">
                    </div>
                    <div className="right-col group-content">
                        <div className="project-info">
                            <button className="btn-primary"></button>
                        </div>
                        <div className="markdown-wrapper">
                            <h1 className="project-title">Description</h1>
                            <p dangerouslySetInnerHTML={{ __html: project.description }} />
                        </div>
                    </div>
                </div>
            </section>
            
            <section className="section-wrapper">
                <div className="section-wrapper-content section-content-padding">
                    <div className="left-col">
                        <span className="subtitle">Images</span>
                    </div>
                    <div className="right-col group-content">
                        <div className="markdown-wrapper info-wrapper no-border">
                            <GridGallery project={project} />
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
};

export default Header05;
