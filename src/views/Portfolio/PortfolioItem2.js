import React, { useState, useEffect } from 'react';
import Header05 from './Header05';
import portfolioData from '../../portfolioData';
import ProjectPagination  from '../../container/ProjectPagination/ProjectPagination';
import './portfolio.scss';
import Section01 from './Section01';

const PortfolioItem2 = (props) => {

    const [project, setProject] = useState(null);
    const [prevProject, setPrevProject] = useState(null);
    const [nextProject, setNextProject] = useState(null);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        
        setLoading(true);
        const findProject = (idxValue) => {

            for (let idx = 0; idx < portfolioData.length; idx++) {
    
                const project = portfolioData[idx];
                if (project.id === idxValue){                
                    return(project);
                }
    
            }
    
        }

        for (let idx = 0; idx < portfolioData.length; idx++) {

            const project = portfolioData[idx];
            if (project.url === props.location.pathname){
                
                setProject(project);

                setNextProject(findProject(project.id +1));
                setPrevProject(findProject(project.id -1));

                setLoading(false);
                break;

            }

        }
        
    }, [props]);


    if (!project)
    {
        return null;
    }

    return (
        <Header05 project={project} />
    );

};

export default PortfolioItem2;