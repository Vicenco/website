import React, {useRef, useEffect} from 'react';
import { TweenMax, TimelineLite, Power3 } from 'gsap'; 
import arrow from '../../assets/arrow-right.svg';
import './header03.scss'

const Header03 = ({project}) => {

    let app = useRef(null);
    let images = useRef(null);
    let content = useRef(null);
    let floorBox = useRef(null);

    let tl = new TimelineLite({delay: .8});

    useEffect(()=> {

        // Image variables
        const girlImage = images.firstElementChild;
        const boyImage = images.lastElementChild;

        // Content variables
        const headlineFirst = content.children[0].children[0];
        const contentP = content.children[1];
        const contentBtn = content.children[2];        

        // Removing initial DOM flash
        TweenMax.to(app, 0, {css: {visibility: 'visible'}});

        // Images animation
        tl.from(girlImage, 1.2, {y: 1280, ease: Power3.easeOut}, 'Start')
            .from(girlImage.firstElementChild, 2 , {scale: 1.6, ease: Power3.easeOut}, .2)

        tl.from(boyImage, 1.2, {y: 1280, ease: Power3.easeOut}, .2)
            .from(boyImage.firstElementChild, 2 , {scale: 1.6, ease: Power3.easeOut}, .2)

        // Floor box
        tl.from(floorBox, 1.2, {y: 10, height: 0, skewType: "simple", skewX: -30,  ease: Power3.easeOut}, .2);

        // Content animations
        tl.staggerFrom([headlineFirst.children], 1, {
            y: 44,
            ease:Power3.easeOut,
            delay: .8
        }, .25, 'Start')
        .from(contentP, 1, { y: 20, opacity: 0, ease: Power3.easeOut}, 1.7)
        .from(contentBtn, 1, { y: 20, opacity: 0, ease: Power3.easeOut}, 2)

    }, [tl]);



    return (
        <div className="hero" ref={el => app = el}>
            <div className="container">
                <div className="hero-inner">
                    <div className="hero-content">
                        <div className="hero-content-inner" ref={el => content = el}>
                            <h1>
                                <div className="hero-content-line">
                                    <div className="hero-content-line-inner">{project.title}</div>
                                </div>
                                <span>{`0${project.id +1}`}</span>
                            </h1>
                            <p dangerouslySetInnerHTML={{ __html: project.description }} />
                            <div className="btn-row">
                                <button className="explore-button">
                                    Play Game
                                    <div className="arrow-icon">
                                        <img src={arrow} alt="arrow" />
                                    </div>
                                </button>
                                
                            </div>
                        </div>
                    </div>
                    <div className="hero-images">
                        <div className="hero-images-inner" ref={el => images = el}>
                            <div className="hero-image girl">
                                <img src="/assets/images/tetris/01.jpg" alt=""/>
                            </div>
                            <div className="hero-image boy">
                                <img src="/assets/images/tetris.jpg" alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="fluid-container infoBox">
                {
                    project && project.projectInfo.map((item, idx) => (
                        <div key={idx} className="project-infobox">
                            <p className="project-description">{item.text}</p>
                        </div>  
                    ))
                }                        
            </div>
            <div className="floorbg" ref={el => floorBox = el} />
        </div> 
    );
};

export default Header03;