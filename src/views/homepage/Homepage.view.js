import React, { useState, useRef, useEffect } from 'react';
import { useSpring, animated } from 'react-spring';
import { TweenMax, TimelineLite, Power3 } from 'gsap'; 

import DotGrid from '../../components/DotGrid/DotGrid';
import SquareBtn from '../../components/Buttons/SquareBtn';
import LatestWork from '../../components/LatestWork/LatestWork';
import Loader from '../../components/Loader/Loader';
import PortfolioData from '../../portfolioData';
import Modal from '../../components/Modal/Modal';

import './homepage.scss';

const Homepage = () => {

    const fade = useSpring({ from: { opacity: 0 }, opacity: 1, config: { duration: 900 } });
    const [showModal, setShowModal] = useState(false);
    const [loaded, hasLoaded] = useState(false);
    
    let app = useRef(null);
    let captionTitle = useRef(null);
    let latestWork = useRef(null);
    let tl = new TimelineLite({delay: .8});
    

    const toggleModal = () => {
        setShowModal(!showModal);
    }


    const closeModal = (oEvent) => {
        
        if(oEvent.target.id === "modal"){
            toggleModal();
        }

    }
    

    useEffect(()=> {

        if (!loaded){
                
            // Image variables
            const subtitle = captionTitle.firstElementChild;
            const headerTitle = captionTitle.children[1];
            const description = captionTitle.children[2];
            const aboutBtn = captionTitle.children[3];

            // Content variables
            const LW_title = latestWork.children[0];
            const LW_Post1i = latestWork.children[1];

            // Removing initial DOM flash
            TweenMax.to(app, 0, {css: {visibility: 'visible'}});        

            // Images animation
            tl.from(subtitle, 1.2, {y: -1000, ease: Power3.easeOut}, 'Start')
                .from(subtitle.firstElementChild, 2 , {scale: 1.6, ease: Power3.easeOut}, .2)

            tl.from(headerTitle, 1.2, {x: -280, opacity:0, ease: Power3.easeOut}, .4)
                .from(headerTitle.firstElementChild, 2 , {scale: 1.6, ease: Power3.easeOut}, .2)

            tl.from(description, 1.6, {y: 80, opacity: 0, ease: Power3.easeOut}, .2)
                .from(description.firstElementChild, 2 , {scale: 1.6, ease: Power3.easeOut}, .5)

            tl.from(aboutBtn, 1.6, {y: 80, opacity: 0, ease: Power3.easeOut}, .2)
                .from(aboutBtn.firstElementChild, 2 , {scale: 1.6, ease: Power3.easeOut}, .7)

                
            const NavBar1stLink = latestWork.children[0];
            const NavBar2ndLink = NavBar1stLink.nextSibling;
            const NavBar3rdLink = NavBar2ndLink.nextSibling;
            const NavBar4thLink = NavBar3rdLink.nextSibling;
            const NavBar5thLink = NavBar4thLink.nextSibling;
            
            // Content animations
            tl.staggerFrom([NavBar2ndLink, NavBar3rdLink, NavBar4thLink, NavBar5thLink], 1, {
                opacity: 0,
                y: 60,
                ease:Power3.easeOut,
                delay: .8
            }, .29, 'Start')


            // Content animations
            tl.from(LW_Post1i, 1, { y: 20, opacity: 0, ease: Power3.easeOut}, .4);

            const timer = setTimeout(() => {
                hasLoaded(true);    
            }, 2700);

            return () => clearTimeout(timer);

        }
        
    }, [tl]);

    return (
        <animated.section style={fade} className="page-container no-select" ref={el => app = el}>
            <DotGrid />
            <Loader />
            <div className="homepage-container home-pt">

                <div className="main-title-header" ref={el => captionTitle = el}>
                    <h3 className="info-title">Designer / Gamer / Developer</h3>
                    <h1 className="title">Digital Developer</h1>
                    <p className="text">I create digital designs & frontend solutions for web sites, applications & for videogame development</p>
                    <SquareBtn label="About Vicenco" url="/about-me" />
                </div>

                <div className="latest-work-container" ref={el => latestWork = el}>
                    <SquareBtn className="button" label="Download resume" close={(e) => toggleModal(e)} />
                    <h2 className="sub-title">Latest Work</h2>
                    {
                        PortfolioData
                        .filter((i, idx) => idx < 3)
                        .map((item, idx) => ( <LatestWork key={idx} item={item} /> ))
                    }
                </div>
            </div>
            {
                showModal && <Modal closeModal={closeModal} title="Download">
                    <p>Click the button below to download my resume</p>
                    <div className="resume-selection">
                        <button>Web resume</button>
                        <button>Videogame resume</button>
                    </div>
                </Modal>
            }
        </animated.section>
    );
};

export default Homepage;