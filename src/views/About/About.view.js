import React, {useRef, useState, useEffect} from 'react';
import { TimelineLite, Power3 } from 'gsap'; 
import { useSpring, animated } from 'react-spring';
import { TabButtonContainer, TabContent } from '../../components/VerticalScroller/verticalScroller.component';
import './about.scss';


const About = () => {

    let image = useRef(null);
    let tl = new TimelineLite({delay: .8});

    const [loaded, hasLoaded] = useState(false);
    const fade = useSpring({ from: { opacity: 0 }, opacity: 1, config: { duration: 900 } });
    const currentYear =  new Date().getUTCFullYear();
    let startYear = 1998;
    startYear = currentYear - startYear;


    useEffect(()=> {

        const selfImage = image;

        if (!loaded){

            // Image animation
            tl.from(selfImage, 1, {x: 180, opacity: 0, ease: Power3.easeOut}, .2);
            
            hasLoaded(true);

        }

    }, [tl]);


    return (

        <animated.div style={fade} className="container no-select">

            {/* Tab links */}
            <TabButtonContainer totalItems={3} />

            {/* Tab content */}
            <TabContent id="tab1" className="tabContent portfolio-item-fullscreen ">
                
                <div className="self-intro">
                    <img className="selfie" src="/assets/images/self.jpg" alt="Me" ref={el => image = el} />
                    {/* <div className="row">
                        <h2 className="section-title">About myself</h2>
                    </div> */}
                    <div className="row">
                        <div className="about-content">
                            {/* <p className="about-intro">My name is Vincenzo Corleone, a digital designer/developer in web sites/applications and indie videogame development.</p>  */}
                            
                            <p className="about-intro">My name is <span className="highlight">Vincenzo</span>, and go by the online name: Dekrayzis. <br/><br/>A multidisciplinary digital designer & developer in both <span className="highlight">web</span> & <span className="highlight">videogame development</span>. </p>
                            <p className="about-intro">Self taught with a combination of <span className="highlight">over {`${startYear}`} years</span> of experience; I focus on creating rich seamless experiences between the product and the user.<br/><br/> I bring a range of skills from:</p>
                            <ul className="skill-set">
                                <li>Architectual visualization & interaction</li>
                                <li>Digital design</li>
                                <li>Prototyping</li>
                                <li>C++ / Javascript programming</li>
                                <li>3D modelling</li>
                                <li>Animation</li>
                                <li>Level design</li>
                                <li>Web applications / sites</li>
                            </ul> 
                            <br/><p className="about-intro"> and thus I'm always looking for creative people to vibe with, let's connect through my social channels below.</p>
                            {/* <p className="about-intro">I’m a <span className="highlight">Game developer</span>, programmer, <span className="highlight">Web designer</span> and <span className="highlight">Web application software engineer</span>. I've also dabbled in Cinematography too!<br />
                    I'm one of those people who likes a hands on approach to things. If I think it, I want to design it. If I design it I want to create it.
                    <br />Web application technology and videogame production has allowed me to do both things and embrace what I like doing most in order to become a more versitial digital creator.</p>

                            <p className="about-intro">
                                <br />As a game developer I've worked with the <span className="highlight">Unreal Engine</span> through its various iterations since 2001, both as an indie developer, <span className="highlight">lead project designer</span> and as consultant to various teams.
                    Over the years I've honed my skills into pretty much all aspects that the engine has to offer.</p>

                            <p className="about-intro">
                                <br />My Path into Web design and development started much sooner working primarily back then in PHP and MySQL before leaving the industry to pursue more creative endevors only to return to it several years later when the technology abled me to feed my hunger for something creative on the web. These days I'm mainly working within a <span className="highlight">React.js</span> application.
                    <br /><br />If you require some assistance or help on a project, feel free to get in contact.</p> */}
                        </div>
                    </div>
                </div>
            </TabContent>


            <TabContent id="tab2" className="tabContent portfolio-item-fullscreen ">
                <div className="section-services section_pt-x2">
                    <h2 className="section-title">What I Know</h2>
                    <div className="row">
                        <div className="workType">
                            <div className="workType-heading">
                                <h6>Web Design</h6>
                            </div>
                            <div className="workType-content">
                                <ul className="workType-list">
                                    <li className="workType-item">
                                        <span className="workType-text">UI / UX Design</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Front-end</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Color psychology</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Wire-framing</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Research &amp; Analytics</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">User scenario</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="workType">
                            <div className="workType-heading">
                                <h6>Graphic Design</h6>
                            </div>
                            <div className="workType-content">
                                <ul className="workType-list">
                                    <li className="workType-item">
                                        <span className="workType-text">Logos</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="workType">
                            <div className="workType-heading">
                                <h6>Web Development</h6>
                            </div>
                            <div className="workType-content">
                                <ul className="workType-list">
                                    <li className="workType-item">
                                        <span className="workType-text">React.js</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Javascript</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">HTML5</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Css / Less / Sass</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Git hub/Lab</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Redux</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">React-Spring</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">SAPUI5</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="workType">
                            <div className="workType-heading">
                                <h6>Video game Production</h6>
                            </div>
                            <div className="workType-content">
                                <ul className="workType-list">
                                    <li className="workType-item">
                                        <span className="workType-text">Unreal engine 4</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Blueprint</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">C++</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Visual FX</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">3D Modelling</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Animation</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Level design</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">In game cinematics</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </TabContent>


            <TabContent id="tab3" className="tabContent portfolio-item-fullscreen ">
                <div className="section-services section_pt-x2">
                    <h2 className="section-title">Software I Use</h2>
                    <div className="row">
                        <div className="workType">

                            <div className="workType-content">
                                <ul className="workType-list">
                                    <li className="workType-item">
                                        <span className="workType-text">3D Studio Max</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Adobe Photoshop</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Adobe XD</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="workType">

                            <div className="workType-content">

                                <ul className="workType-list">
                                    <li className="workType-item">
                                        <span className="workType-text">Adobe Illustrator</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Unreal Engine 4</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Rizom Virtual spaces</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Substance Designer</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="workType">
                            <div className="workType-content">
                                <ul className="workType-list">
                                    <li className="workType-item">
                                        <span className="workType-text">Substance Painter</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Visual Studio 2017</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Amazon web services</span>
                                    </li>
                                    <li className="workType-item">
                                        <span className="workType-text">Inkscape</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="workType">
                            <div className="workType-content">
                                <ul className="workType-list">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </TabContent>            
            <h2 className="page-label">About me</h2>
        </animated.div>
    );


};

export default About;