import React from 'react';
import Tilt from 'react-vanilla-tilt';
import './latestWork.scss';

const LatestWork = ({ item }) => {

    const { title, type, mainImage, url } = item;
    

    const updateCss = (bShow, boxClass) => {

        const item = document.getElementsByClassName(boxClass)[0];

        if (item){
            if (bShow){

                item.style.backgroundImage = `url(${mainImage})`;
                item.classList.remove("fadeOut");
                item.classList.add("fadeIn");

            } else {
                item.classList.remove("fadeIn");
                item.classList.add("fadeOut");
            }
        }

    };


    return (
        <a href={url} className="latest-work">
            <Tilt className="latest-work-header" onMouseOver={() => updateCss(true, `image-${title}`) } onMouseLeave={() => updateCss(false, `image-${title}`) }>
                <h2 className="latest-work-title">{title}</h2>
                <span className="latest-work-type">{type[0].title}</span>
                <div className={`latest-work-img image-${title} fadeOut`}>
                    <div className="img-overlay" />
                </div>
            </Tilt>
        </a>
    );

};

export default LatestWork;