import React, {useRef, useEffect} from 'react';
import { TimelineLite, Power3 } from 'gsap'; 

import './logo.styles.scss';

const Logo = () => {

    let logoRef = useRef(null);

    let tl = new TimelineLite({delay: .8});

    useEffect(()=> {
        
        if(logoRef){
            tl.from(logoRef, 1, { y: -20, opacity: 0, ease: Power3.easeOut}, .7)
        }
    
    }, [tl]);

    return (
        <div className="logo-block no-select">
            <div className="site-logo" ref={el => logoRef = el}>
                <a href="http://vicenco.co.uk/">
                    <span className="siteName">VICENCO</span>
                    <span className="slogan">Designer / Gamer / Developer</span>
                </a>
            </div>
        </div>
    );
};

export default Logo;