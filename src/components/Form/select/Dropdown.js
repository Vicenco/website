import React, { useState } from 'react';

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];
const Dropdown = () => {

  const [selectedOption, setSelectedOption] = useState(null);


  const handleChange = (selectedOption) => {
    setSelectedOption(selectedOption);
    console.log(`Option selected:`, selectedOption);
  };

  return (
    <select value={selectedOption} onChange={this.handleChange}>
      options={options}
    </select>
  );

};

export default Dropdown;