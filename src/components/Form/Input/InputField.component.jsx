import React from 'react';
import './inputField.styles.scss';

const InputField = ({ value, placeholder }) => {
    return (
        <input type="text" value={value} placeholder={placeholder} />
    );
};

export default InputField;