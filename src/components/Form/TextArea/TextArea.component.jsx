import React from 'react';
import './textArea.styles.scss';

const InputField = ({ value, placeholder, columns, rows }) => {
    return (
        <textarea value={value} placeholder={placeholder} cols={columns} rows={rows} />
    );
};

export default InputField;