import React, { useRef, useEffect } from "react";
import './lineborders.scss';

const LineBorders = () => {

    const canvasRef = useRef();

    useEffect(() => {

        function getDocumentWidth() {
            return Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        };

        function getDocumentHeight() {
            return Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
        };

        const currentCanvas = canvasRef.current;
        const context = currentCanvas.getContext("2d");
        
        var vw = currentCanvas.width = getDocumentWidth(),
        vh = currentCanvas.height = getDocumentHeight();
        
        
        // resize the canvasRef to fill the browser window
        window.addEventListener('resize', onResize, false);

        function onResize() {
            resizeCanvas();
        }

        function resizeCanvas() {

            currentCanvas.width = vw;
            currentCanvas.height = vh;
            drawGrid();

        }
        resizeCanvas();


        // grid
        function drawGrid() {

            var cellW = 490;

            // vertical lines
            for (var x = 0; x <= vw; x += cellW) {
                context.moveTo(x, 0); // x, y
                context.lineTo(x, vh);
            }

            context.strokeStyle = "#cccccc";
            context.stroke();
        }
        drawGrid();

    });


    return (
        <canvas ref={canvasRef} className="lineBorderBg" />
    );

};

export default LineBorders;