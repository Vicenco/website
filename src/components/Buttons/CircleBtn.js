import React from 'react';
import './buttons.scss';

const CircleBtn = ({icon, callback}) => {

    const classNames = `icon ${icon}`;
    return (
        <button className="btn-circle" onClick={callback}>
            <i className={classNames} />
        </button>
    );
};

export default CircleBtn;