import React, {useRef, useState, useEffect} from 'react';
import { TimelineLite, Power3 } from 'gsap'; 

import './buttons.scss';

const SquareBtn = ({label, url, close}) => {

    let sBtn = useRef(null);
    let sLink = useRef(null);
    const [loaded, hasLoaded] = useState(false);

    let tl = new TimelineLite({delay: .8});

    useEffect(()=> {
        
        if(!loaded){
            if(sBtn && sBtn.children){
                tl.from(sBtn, .5, { y: -10, opacity: 0, ease: Power3.easeOut}, 1.2)
            }
            
            if(sLink && sLink.children){
                tl.from(sLink, 1, { y: 20, opacity: 0, ease: Power3.easeOut}, 1)    
            }
            
            hasLoaded(true);
        }

    }, [tl]);

    return (
        <>
        {
            close ? (
                
            <button className="btn-square" onClick={close} ref={el => sBtn = el}>
                {label}
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </button>
            ) : (
                <a href={url} className="btn-square" ref={el => sLink = el}>
                    {label}
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            )
        }
        </>
    );
};

export default SquareBtn;