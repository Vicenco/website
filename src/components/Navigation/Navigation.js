import React, {useRef, useState, useEffect} from 'react';
import { TweenMax, TimelineLite, Power3 } from 'gsap'; 
import './nav.scss';

const MyMenu = [{
    label: "Home",
    url:"/"
},{
    label: "Portfolio",
    url:"/portfolio"
},{
    label: "About me",
    url:"/about-me"
},{
    label: "Blog",
    url: null
},{
    label: "Contact",
    url:"/contact"
},{
    label: "Tutorials",
    url: null
},{
    label: "Store",
    url: null
}]

const Navigation = () => {

    let navBar = useRef(null);
    let tl = new TimelineLite({delay: .8});
    const [loaded, hasLoaded] = useState(false);

    useEffect(()=> {
        
        if (!loaded){
            const NavBar1stLink = navBar.children[0];
            const NavBar2ndLink = NavBar1stLink.nextSibling;
            const NavBar3rdLink = NavBar2ndLink.nextSibling;
            const NavBar4thLink = NavBar3rdLink.nextSibling;
            
            // Content animations
            tl.staggerFrom([NavBar1stLink, NavBar2ndLink, NavBar3rdLink, NavBar4thLink], 1, {
                opacity: 0,
                x: 100,
                ease:Power3.easeOut,
                delay: .8
            }, .25, 'Start')
        
            hasLoaded(true);
        }

    },[tl])



    const renderMenu = () => {

        let menuArray = []
        MyMenu.map((item, idx) => {
                           
            if(item.url != null){
                menuArray.push(
                    <li key={idx} className="nav-item menu-item-depth-0">
                        <a href={item.url} className="menu-link main-menu-link item-title">
                            <span>{item.label}</span>
                        </a>
                    </li>
                )
            }

        });

        return menuArray;

    };

    return (
        <div className="main-nav-container" role="navigation">
            <ul id="primary-menu" className="menu" ref={el => navBar = el}>
                { renderMenu() }
            </ul>
        </div>
    );
    
}

export default Navigation;