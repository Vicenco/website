import React from 'react';

const MetaData = ({tags, date}) => {
    return (
        <div className="portfolio-details-categories">
            <div className="category-holder">
                {
                    tags ? tags.map((tag, idx) => (
                        <span className="category" key={idx}>{tag}{idx === tags.length - 1 ? '' : ', '}</span>
                    )) : null
                }
            </div>
            <span className="portfolio-details-date ">{date}</span>
        </div>        
    );
};

export default MetaData;