import React from 'react';
import './metadata.scss';

const MetaData = ({tags, date}) => {
    return (
        <div className="project-details-meta">
            {
                tags ? tags.map((tag, idx) => (
                    <span className="category" key={idx}>{tag}</span>
                )) : null
            }
        </div>        
    );
};

export default MetaData;