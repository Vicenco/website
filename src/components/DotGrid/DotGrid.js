import React, { useRef, useEffect } from "react";

const DotGrid = () => {

    const canvasRef = useRef();

    useEffect(() => {

        const currentCanvas = canvasRef.current;
        const context = currentCanvas.getContext("2d");

        var vw = currentCanvas.width = 670,
            vh = currentCanvas.height = 530;    
        
            // resize the canvasRef to fill the browser window
        window.addEventListener('resize', onResize, false);

        function onResize() {
            resizeCanvas();
        }

        function resizeCanvas() {

            currentCanvas.width = vw;
            currentCanvas.height = vh;
            drawDots();

        }
        resizeCanvas();

        // Dots
        function drawDots() {
            var r = 2,
                cw = 20,
                ch = 20;

            for (var x = 20; x < vw; x += cw) {
                for (var y = 20; y < vh; y += ch) {
                    context.fillStyle = '#ffffff17';
                    context.fillRect(x - r / 2, y - r / 2, r, r);
                }
            }
        }

        drawDots();

    });


    return (
        <canvas ref={canvasRef} className="dot-grid" />
    );

};

export default DotGrid;