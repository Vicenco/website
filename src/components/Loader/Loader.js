import React, { useState, useRef, useEffect } from 'react';
import { TweenMax, TimelineLite, Power3 } from 'gsap'; 
import './loader.scss';

const Loader = () => {

    let rainbow = useRef(null);
    let tl = new TimelineLite({delay: .8});

    useEffect(()=> {

        TweenMax.to(rainbow.firstElementChild, 2000, {css: {visibility: 'visible'}});

    }, [tl]);


    return (
        <div className="loader-container" ref={el => rainbow = el}>
            <div className="loader">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

    );
};

export default Loader;