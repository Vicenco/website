import React, { useEffect, useRef } from "react";
import { TweenMax, TimelineLite, Power3 } from "gsap/all";
import './lightbox.scss';

const LightBox = ({closeBox, image }) => {
    
    let currentLightBox = useRef(null);
    let tl = new TimelineLite();

    useEffect(()=> {
        
        const imageBox = currentLightBox.firstElementChild;

        TweenMax.to(imageBox, 0, {css: {visibility: 'visible'}});  
        TweenMax.to(currentLightBox, 0, {css: {visibility: 'visible'}});  

        // Content animations
        tl.from(currentLightBox, .3, { opacity: 0, ease: Power3.easeInOut}, .6);
        tl.from(imageBox, 1, { y: 20, ease: Power3.easeOut}, .6);

    }, [tl]);
    

    return (
        <div id="lightBox" className="lightbox-wrapper " onClick={closeBox} ref={el => currentLightBox = el}>
			<div className="lightbox-inner">
				<div className="lightbox-content">
					<img src={image} alt="image" />
				</div>
			</div>
		</div>
    );

};

export default LightBox;
