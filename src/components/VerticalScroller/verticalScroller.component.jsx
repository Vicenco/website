import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

import Scroller from './Scroller/Scroller';
import './verticalScroller.scss';

/* Style the overlay container */
export const TabButtonStyle = styled.button`
    display: -webkit-inline-box;
    display: -webkit-inline-flex;
    display: -ms-inline-flexbox;
    display: inline-flex;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
    justify-content: center;
        -webkit-box-align: center;
        -webkit-align-items: center;
        -ms-flex-align: center;
    align-items: center;
    background: rgba(0, 0, 0, 0.3);
    height: 100%;
    cursor: pointer;
        -webkit-transition: all cubic-bezier(0.4, 0, 0.2, 1) 0.4s;
        -o-transition: all cubic-bezier(0.4, 0, 0.2, 1) 0.4s;
    transition: all cubic-bezier(0.4, 0, 0.2, 1) 0.4s; }
    border: 0;
    box-sizing: border-box;
        -ms-word-wrap: break-word;
    word-wrap: break-word;
    font-size: 0.94rem;
    font-weight: 500;
    color: #fff;
    color: white;
    transition: 0.25s;
`;

export const TabButtonContainerStyle = styled.div`
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
    flex-direction: column;
        -webkit-justify-content: space-around;
        -ms-flex-pack: distribute;
    justify-content: space-around;
    right: 0;
    top: 0;
    position: fixed;
    width: 40px;
    height: 100%;
    z-index: 10;
    transition: 0.25s;
`;

export const TabContentStyle = styled.div`
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    color: white;
    transition: 0.25s;
`;


export const TabButtonContainer = ({ children, totalItems }) => {

    const [activeId, setActiveId] = useState(0);
    const [state, setState] = useState({ scrollTop: 0 });

    useEffect(function subscribeToWheelEvent() {

        const updateScroll = function (e) {

            if (!!e.deltaY) {

                setState(() => {

                    const delta = Math.sign(e.deltaY) * 1.0;
                    let tablink = document.getElementsByClassName('tablink');
                    let newIdx = activeId > 0 ? activeId : 0;

                    for (let i = 0; i < tablink.length; i++) {
                        
                        if (tablink[i].className.includes('active')){
                            newIdx = i;
                            break;
                        }

                    }
                    
                    if(delta < 0){
                        newIdx = newIdx -1;
                    } else {
                        newIdx = newIdx +1;
                    }

                    setActiveId(newIdx);                    
                    OpenTab(e, newIdx);

                });

            }

        };

        window.addEventListener('mousewheel', updateScroll);

        return function () {
            window.removeEventListener('mousewheel', updateScroll);
        }

    }, []);



    const OpenTab = (e, tabId) => {

        let i, tabcontent, tablink;

        // Hide all tab contents when tab button is clicked
        tabcontent = document.getElementsByClassName('TabContentStyle');
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        tablink = document.getElementsByClassName('tablink');
        for (i = 0; i < tabcontent.length; i++) {
            tablink[i].className = tablink[i].className.replace('active', "");
        }


        if (tabcontent[tabId]) {
            tabcontent[tabId].style.display = "block";
            tablink[tabId].className = tablink[tabId].className += ' active';
        }


        if (tabId > totalItems - 1) {
            tabId = 0;
            tabcontent[0].style.display = "block";
            tablink[0].className = tablink[0].className += ' active';
        }


        if (tabId < 0) {
            tabId = totalItems - 1;
            tabcontent[tabId].style.display = "block";
            tablink[tabId].className = tablink[tabId].className += ' active';
        }

        setActiveId(tabId);

    };


    const OnButtonClicked = (e) => {

        const currentBtn = e.currentTarget
        let newIndex;

        switch (currentBtn.id) {

            case 'leftSlideBtn':
                newIndex = activeId - 1;
                break;

            case 'rightSlideBtn':
                newIndex = activeId + 1;
                break;

            default:
                return;
        }

        OpenTab(e, newIndex);

    };

    const createTabs = () => {

        let tabs = [];

        for (let idx = 0; idx < totalItems; idx++) {
            const classNames = idx === 0 ? "tablink active" : "tablink";
            tabs.push(<TabButtonStyle key={idx} id={`tab${idx + 1}`} className={classNames} onClick={(event) => OpenTab(event, idx)}>{`0${idx + 1}`}</TabButtonStyle>)
        }
        return tabs
    }


    return (
        <TabButtonContainerStyle>
            <Scroller activeIdx={activeId} totalAmount={totalItems} />
            {createTabs()}
            <SliderButtons updateIndx={(e) => OnButtonClicked(e)} />
        </TabButtonContainerStyle>
    );

};


export const SliderButtons = ({ updateIndx }) => {

    return (
        <div className="twin-container slider-btns">
            <button id="leftSlideBtn" className="btn-round btn-round-light" onClick={updateIndx}>
                <i className="demo-icon icon-left-open-1" />
            </button>

            <button id="rightSlideBtn" className="btn-round btn-round-light" onClick={updateIndx}>
                <i className="demo-icon icon-right-open-1" />
            </button>
        </div>
    );
};


export const TabContent = ({ id, children }) => {

    return (
        <TabContentStyle id={`${id}`} className="TabContentStyle portfolio-item-fullscreen" style={{ display: id === "tab1" ? 'block' : 'none' }}>
            {children}
        </TabContentStyle>
    );
};

export default TabButtonContainer;