import React, { useState } from 'react';
import { useSpring, animated } from 'react-spring';
import useMeasure from './useMeasure';
import './scroller.css';

const Scroller = ({ activeIdx, totalAmount }) => {
    const [bind, { width }] = useMeasure();
    const props = useSpring({
        width: activeIdx ? `${(activeIdx +1) / totalAmount * 100}}%` : "25%",
        config: { duration: 300 }
    });
    
    return (
        <div {...bind} className="clb-scroll-top clb-slider-scroll-top vc_hidden-md vc_hidden-sm vc_hidden-xs">
            <div className="clb-scroll-top-bar">
                <animated.div className="fill"/>
                <animated.div className="scroll-track" style={props} />
            </div>
            <div className="clb-scroll-top-holder font-titles">
                Scroll									
            </div>
        </div>
    );
};

export default Scroller;