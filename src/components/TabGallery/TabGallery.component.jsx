import React, { useState, useEffect, useRef } from 'react';
import { Link } from "react-router-dom";
import PortfolioData from '../../portfolioData';
import './tabGallery.scss';


export const TabGalleryButtonContainer = ({ title }) => {

    const [activeId, setActiveId] = useState(0);

    const OpenTab = (e, tabId, filter) => {

        let i, tabcontent, tablink;

        // Hide all tab contents when tab button is clicked
        if (filter) {

            tabcontent = document.getElementsByClassName('portfolio-showcase');
            for (i = 0; i < tabcontent.length; i++) {

                if (filter === "*"){
                    tabcontent[i].style.display = "flex";
                } else {

                    if (!tabcontent[i].className.includes(filter)) {
                        tabcontent[i].style.display = "none";
                    } else {
                        tabcontent[i].style.display = "flex";
                    }

                }

            }

        }

        tablink = document.getElementsByClassName('btn-filter');
        for (i = 0; i < tablink.length; i++) {
            tablink[i].className = tablink[i].className.replace('active', "");
        }

        tablink[tabId].className = tablink[tabId].className += ' active';
        // tabcontent[tabId].className = tabcontent[tabId].className += ' active';
        setActiveId(tabId);

    };


    const getFilters = () => {

        let filterArray = [];
        for (let idx = 0; idx < PortfolioData.length; idx++) {
            filterArray.push(PortfolioData[idx].filter);
        }
        return [...new Set(filterArray)];

    };


    const createTabs = () => {

        let tabs = [];
        tabs.push(<button key="001" className="btn-filter active" data-filter="*" data-hover="all" onClick={(event) => OpenTab(event, 0, "*")}>All</button>);
        
        const filters = getFilters();
        for (let idx = 0; idx < filters.length; idx++) {

            tabs.push(<button
                key={idx+1}
                className="btn-filter"
                data-filter={filters[idx]}
                onClick={(event) => OpenTab(event, idx + 1, filters[idx])}>{filters[idx].charAt(0).toUpperCase() + filters[idx].slice(1)}</button>
            );

        }

        return tabs
    }

    return (
        <div className="tab-gallery-btn-container">
            <h2 className="tab-gallery-title ">{title}</h2>
            {createTabs()}
            <TabContent id={activeId} />
        </div>
    );

};

const GridItem = ({project, loaded}) => {

    var classArray = [
        'horizontal'
    ];
    var randomClass = Math.floor(Math.random() * classArray.length);

    return (
        <Link to={project.url} className={`portfolio-showcase ${classArray[randomClass]} ${project.filter}`} >
            <img  />
            <div className="case-study__overlay">
                <h2 className="case-study__title">{project.title}</h2>
                {/* <span className="case-study__desc">View project details</span> */}
            </div>
        </Link>
    );

};

export const TabContent = ({ id }) => {

    const createContent = () => {

        let content = [];
        for (let idx = 0; idx < PortfolioData.length; idx++) {
            content.push(<GridItem key={idx} project={PortfolioData[idx]} />);
        }
        return content;

    }

    return (
        <div id={`${id}`} className="filter-wrapper-mix grid-gallery">
            {createContent()}
        </div>
    );
};

export default TabGalleryButtonContainer;