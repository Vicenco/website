import React, { useEffect, useRef } from "react";
import { TweenMax, TimelineLite, Power3 } from "gsap/all";
import './modal.scss';

const Modal = ({closeModal, children, title }) => {
    
    let currentModal = useRef(null);
    let tl = new TimelineLite();

    useEffect(()=> {
        
        const innerModal = currentModal.firstElementChild;

        TweenMax.to(innerModal, 0, {css: {visibility: 'visible'}});  
        TweenMax.to(currentModal, 0, {css: {visibility: 'visible'}});  

        // Content animations
        tl.from(currentModal, .3, { opacity: 0, ease: Power3.easeInOut}, .6);
        tl.from(innerModal, 1, { y: 20, ease: Power3.easeOut}, .6);

    }, [tl]);
    

    return (
        <div className="modal-wrapper " onClick={closeModal} ref={el => currentModal = el}>
			<div className="modal-inner">
				<div className="modal-content">
					<div className="modal-header">
						<h2 className="modal-title">{title}</h2>
					</div>
					<div className="modal-body">
						{children}
					</div>
					<div className="modal-footer">
						<button id="modal" className="btn btn-secondary" onClick={closeModal}>
							Close
						</button>
					</div>
				</div>
			</div>
		</div>
    );

};

export default Modal;
