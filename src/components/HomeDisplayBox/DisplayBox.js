import React from 'react';
import Tilt from 'react-vanilla-tilt';
import MetaData from '../Metadata/MetaData';

const DisplayBox = ({project}) => {

    return (
        <div className="portfolio-bg-overlay" style={{ backgroundImage: `url(${project.mainImage})`}}>
            <div className="portfolio-item-bg-title">
                <span className="bg-title">{project.title}</span>
            </div>
            <div className="page-container details-holder">
                <div className="portfolio-details">
                    <MetaData tags={project.tags} date={project.date} />                    
                    <div className="portfolio-details-title">
                        <a href={project.url}>
                            <h2 className="portfolio-details-headline title ">{project.title}</h2>
                        </a>
                    </div>
                    <div className="portfolio-details-description">
                        <div className="short-description " dangerouslySetInnerHTML={{ __html: project.description }} />
                    </div>
                    <div className="portfolio-details-link">
                        <a className="btn btn-link btn-lightbox " href={project.url}>
                            View Project
                            <i className="ion-right ion" />
                        </a>
                    </div>
                </div>
            </div>
            
            <Tilt className="portfolio-item-preview">
                <div className="portfolio-item-image parallax" style={{ backgroundImage: `linear-gradient(to right, rgba(26,25,29, 1.0), rgba(26,25,29, 0.3)), url(${project.mainImage})`}}></div>
            </Tilt>
        </div>
    );
};

export default DisplayBox;