import React, { useState } from "react";
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import CircleBtn from './components/Buttons/CircleBtn';
import LineBorders from './components/LineBorders/LineBorders';
import Footer from './container/Footer/Footer';
import './styles/App.scss';
import "./fontello/css/fontello.css"

import Header from './container/Header/Header';
import Homepage from './views/homepage/Homepage.view';
import Portfolio from './views/Portfolio/Portfolio.view';
import PortfolioItem2 from "./views/Portfolio/PortfolioItem2";
import PortfolioItem3 from "./views/Portfolio/PortfolioItem3";
import About from './views/About/About.view';
import Contact from './views/Contact/Contact.view';

// Overlays
import { CanvasOverlay } from './container/canvasPanel.styles';
import MenuPopover from './container/MenuPopover/MenuPopover';

function App() {

	const [showMenu, setShowMenu] = useState(false);

	/**
     * Toggle the Projects overlay.
     */
    const toggleShowMenu = () => {
        setShowMenu(!showMenu);
    };

	return (
		<>
			<LineBorders />
			<div className="grid-container">
				<div className="Sidebar">
					<CircleBtn icon="icon-menu" callback={toggleShowMenu}/>
				</div>
				<div className="ContentWindow">
					<Header/>
					<BrowserRouter>
						<div className="maincontent">
							<Switch>
								<Route exact path='/' component={Homepage} />
								<Route exact path='/portfolio' component={Portfolio} />
								<Route exact path='/portfolio/1/:id' component={PortfolioItem2} />
								<Route exact path='/portfolio/2/:id' component={PortfolioItem3} />
								<Route exact path='/about-me' component={About} />
								<Route exact path='/contact' component={Contact} />
							</Switch>	
						</div>
					</BrowserRouter>				
					<Footer />
				</div>
				<CanvasOverlay display={ showMenu ? '0.98' : '0'} depth={ showMenu ? '999' : '-1'} showCanvas={toggleShowMenu}>
                	<MenuPopover hideCanvas={toggleShowMenu} />
            	</CanvasOverlay>
			</div>
		</>

	);

}

export default App;
